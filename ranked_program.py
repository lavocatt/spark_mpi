from yggdrasil.task_lib import Task
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskBag
from yggdrasil.task_lib import Ventilator
from yggdrasil import consts
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class RankedApp(Task) :

    def __init__(self, name,
                 node_list,
                 program,
                 nb_instances,
                 get_options_function,
                 jail_name = "ranked_app",
            timeout=-1, root_r=0, root_n="root"):
        Task.__init__(self, name, timeout, root_r, root_n)
        self.node_list            = node_list
        self.program              = program
        self.jail_name            = jail_name
        self.nb_instances         = nb_instances
        self.get_options_function = get_options_function

    def init_state(self) :
        lst_task=[]
        group = MPIJail(self.jail_name,self.jail_name,self.node_list,'0', 'root')
        list_dependency_running = []
        list_dependency_error = []
        list_dependency_done = []
        for r in range(1, self.nb_instances+1) :
            program_options = self.get_options_function(r)
            pgm = MPIExecutor("{}.{}".format(self.jail_name,r),
                                        ("{} {}"
                                        ).format(
                                            self.program,
                                            program_options),
                    group,True, -1, consts.encoding, False,
                    "{}".format(r),)
            pgm.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(group, consts.INIT_RUNNING)
                    ]
                    )
                )
            pgm.set_trigger_for_transition(consts.INIT_CANCELED,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(group, consts.INIT_ERROR)
                    ]
                    )
                )
            list_dependency_done.append(DependencyEvent(pgm, consts.RUNNING_DONE))
            list_dependency_running.append(DependencyEvent(pgm, consts.INIT_RUNNING))
            list_dependency_error.append(DependencyEvent(pgm, consts.INIT_CANCELED))
            lst_task.append(pgm)
        group.set_dependency_for_transition(
                consts.RUNNING_DONE,
                Dependency(Dependency.AND,list_dependency_done)
                )
        lst_task.append(group)
        self.framework.add_start_all(lst_task)
        self.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND, list_dependency_running))
        self.set_dependency_for_transition(consts.RUNNING_DONE,
                Dependency(Dependency.AND, list_dependency_done))
        self.set_dependency_for_transition(consts.RUNNING_DONE,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(group, consts.RUNNING_DONE)
                    ]
                    )
                )
        self.set_trigger_for_transition(consts.INIT_ERROR,
                Dependency(Dependency.OR, list_dependency_error)
                )
        self.request_transition(consts.INIT_RUNNING)
