#!/usr/bin/env python3
import sys
import os
from yggdrasil.erebor       import runner
from g5k_fetch_spark_folder import G5K

# Path
G5K.base_path    = "/home/user1/bin/spark/spark-2.2.0-bin-hadoop2.7"
G5K.bin_path     = "{}/bin".format(G5K.base_path)
G5K.sbin_path    = "{}/sbin".format(G5K.base_path)
G5K.username     = "user1"
G5K.output_dir   = "/home/{}/Documents/these/".format(G5K.username)

# Deployment control
G5K.nbClient           = 1    # How many nodes are reserved to be executors
G5K.sender_base_port   = 9999 # On wich port should the queue listen on output connexion ?
G5K.receivers_by_sender= 1    # How many receivers/producers are spawned
G5K.nbqueus            = 1    # How many nodes are Queus
G5K.nodesimu           = 2    # How many nodes are simulating
G5K.mpi_process_per_n  = 2    # How many MPI

if __name__ == "__main__":
    sys.exit(runner(sys.argv, G5K))
