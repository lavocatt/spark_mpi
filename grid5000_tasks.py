from threading import Timer
import mcr
from mcr.libmcr import MCCClient
from yggdrasil.task_lib import Task
from yggdrasil import consts
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent
from yggdrasil import Event

class MakeReservation(Task) :

    def __init__(self, name,
                 username,
                 password,
                 cluster_name,
                 site,
                 nb_nodes,
                 walltime,
                 recover_id="",
                 queue="default",
            timeout=-1, root_r=0, root_n="root"):
        Task.__init__(self, name, timeout, root_r, root_n)
        self.nb_nodes = nb_nodes
        self.walltime = walltime
        self.username = username
        self.password = password
        self.cluster_name = cluster_name
        self.site = site
        self.queue = queue
        self.session = mcr.libsession.create_session(
                "https://api.grid5000.fr/",username,password)
        self.recover_id = ""

    def init_state(self) :
        if self.recover_id == "" :
            self.job_id = jid =  MCCClient.job_add(
                    "",
                    self.walltime,
                    [],
                    [],
                    "off",
                    None,
                    self.cluster_name,
                    self.session,
                    self.nb_nodes,
                    self.queue)
        else :
            self.job_id = self.recover_id
        print(self.job_id)
        self.wait_sec =0
        self.timer2 = Timer(5,self.check_reservation_state)
        self.request_transition(consts.INIT_RUNNING)

    def runing_state(self) :
        self.timer2.start()

    def check_reservation_state(self) :
        self.framework.erebor.add_to_queue(Event(self._check_state, self.name))

    def _check_state(self, name="") :
        self.wait_sec += 5
        state = MCCClient.get_job_state(self.session, self.job_id, [self.site])
        if state == "running" :
            self.request_transition(consts.RUNNING_DONE)
        elif state == "error" :
            self.request_transition(consts.RUNNING_ERROR)
        else :
            print("waiting {}".format(self.wait_sec))
            self.timer2 = Timer(5,self.check_reservation_state)
            self.timer2.start()

class DeployEnv(Task) :

    def __init__(self, reservation,
            system_image,
            notification_mail,
            done_callback,
            fail_treshold = 0,
            timeout=-1, root_r=0, root_n="root"):
        """
        :param reservation: a reservation
        :type reservation: MakeReservation
        """
        Task.__init__(self, "ddepl", timeout, root_r, root_n)
        self.reservation = reservation
        self.system_image = system_image
        self.done_callback = done_callback
        self.notification_mail = notification_mail
        self.fail_treshold = fail_treshold
        self.lst_ok_nodes = []
        self.lst_nok_nodes = []
        self.lst_done = []
        self.set_dependency_for_transition(consts.IDLE_INIT,
            Dependency(
                Dependency.AND,
                [
                    DependencyEvent(reservation, consts.RUNNING_DONE)
                ]
            )
        )
        self.set_trigger_for_transition(consts.IDDLE_CANCEL,
            Dependency(
                Dependency.OR,
                [
                    DependencyEvent(reservation, consts.RUNNING_ERROR),
                    DependencyEvent(reservation, consts.INIT_ERROR)
                ]
            )
        )

    def init_state(self) :
        self.list_nodes = lst_nodes = MCCClient.job_host_list(
                self.reservation.session,
                self.reservation.job_id,
                self.reservation.site)
        parts = len(self.list_nodes) // 10
        rest  = len(self.list_nodes) % 10
        if rest > 0 :
            parts+=1
        self.list_depls = []

        for i in range(0, parts) :
            mynds = self.list_nodes[i*10:(i+1)*10] if i < parts-1 else self.list_nodes[i*10:]
            print(mynds)
            depl_id = MCCClient.dep_add(
                    self.reservation.session,
                    self.reservation.job_id,
                    self.reservation.site,
                    mynds,
                    self.system_image,
                    self.notification_mail,
                    "")
            self.list_depls.append(depl_id)
            self.lst_done.append(None)
        self.wait_sec =0
        self.timer2 = Timer(5,self.check_depl_state)
        self.request_transition(consts.INIT_RUNNING)
        self.error_count = 0

    def check_depl_state(self) :
        self.framework.erebor.add_to_queue(Event(self._check_state, self.name))

    def runing_state(self) :
        self.timer2.start()

    def _check_state(self, name="") :
        self.wait_sec += 5
        stop = True
        error = False
        i = 0
        for depl_id in self.list_depls :
            if self.lst_done[i] is None :
                res = self._check_id(depl_id)
                self.lst_done[i] = res
            i+=1
        for state in self.lst_done :
            if state is None :
                stop = stop & False
            elif state is consts.RUNNING_DONE :
                stop = stop & True
            elif state is consts.RUNNING_ERROR :
                stop = stop & True
                error = True
        if stop :
            if error :
                self.request_transition(consts.RUNNING_ERROR)
            else :
                self.done_callback()
                self.request_transition(consts.RUNNING_DONE)
        else :
            self.timer2 = Timer(5,self.check_depl_state)
            self.timer2.start()

    def _check_id(self, depl_id):
        try:
            state = MCCClient.get_dep_state(depl_id, self.reservation.site,
                    self.reservation.session)
            if state == "terminated" :
                raw = MCCClient.get_dep_raw(depl_id,
                        self.reservation.site,
                        self.reservation.session)
                lst_nodes = raw["nodes"]
                result    = raw["result"]
                for node in lst_nodes :
                    res = result[node]
                    if res["state"] == "OK" :
                        self.lst_ok_nodes.append(node)
                    else :
                        self.lst_nok_nodes.append(node)
                print("DONE {} {}".format(depl_id, self.wait_sec))
                return(consts.RUNNING_DONE)
            elif state == "error" :
                raw = MCCClient.get_dep_raw(depl_id,
                        self.reservation.site,
                        self.reservation.session)
                print(raw)
                print("ERROR {} {}".format(depl_id, self.wait_sec))
                return (consts.RUNNING_ERROR)
            else :
                print("WAITING {} {}".format(depl_id, self.wait_sec))
                return (None)
        except Exception as e :
            print(e)
            print("REST ERROR {}".format(self.wait_sec))
            return (None)

class RemoveReservation(Task) :

    def __init__(self, reservation,
            timeout=-1, root_r=0, root_n="root"):
        """
        :param reservation: a reservation
        :type reservation: MakeReservation
        """
        Task.__init__(self, "remove_reserv", timeout, root_r, root_n)
        self.reservation = reservation

    def init_state(self) :
        MCCClient.job_del([self.reservation.job_id],
                self.reservation.session,
                self.reservation.site)
        self.request_transition(consts.INIT_RUNNING)
