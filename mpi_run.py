from yggdrasil.task_lib import Task
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskBag
from yggdrasil.task_lib import Ventilator
from yggdrasil import consts
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class MPIRun(Task) :

    def __init__(self, name,
                 node_list,
                 mpi_program,
                 mpi_run_options,
                 mpi_program_options = "",
                 jail_name = "mpi_app",
            timeout=-1, root_r=0, root_n="root"):
        Task.__init__(self, name, timeout, root_r, root_n)
        self.node_list           = node_list
        self.mpi_program         = mpi_program
        self.mpi_run_options     = mpi_run_options
        self.mpi_program_options = mpi_program_options
        self.jail_name           = jail_name

    def init_state(self) :
        lst_task=[]
        mpi_group = MPIJail(self.jail_name,self.jail_name,self.node_list, '0', 'root')
        mpi_run   = MPIExecutor("mpi_run",
                                    (
                                        "mpirun --host {} {} {}{}"
                                    ).format(
                                        self.node_list,
                                        self.mpi_run_options,
                                        self.mpi_program,
                                        self.mpi_program_options
                                        ),
                mpi_group,True, -1, consts.encoding, False, "0")
        mpi_run.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(mpi_group, consts.INIT_RUNNING)
                    ]
                    )
                )
        mpi_run.set_trigger_for_transition(consts.INIT_CANCELED,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(mpi_group, consts.INIT_ERROR)
                    ]
                    )
                )
        lst_task.append(mpi_group)
        lst_task.append(mpi_run)
        self.framework.add_start_all(lst_task)
        self.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(mpi_run, consts.INIT_RUNNING)
                    ]
                    )
                )
        self.set_dependency_for_transition(consts.RUNNING_DONE,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(mpi_run, consts.RUNNING_DONE)
                    ]
                    )
                )
        self.set_trigger_for_transition(consts.INIT_ERROR,
                Dependency(Dependency.OR,
                    [
                        DependencyEvent(mpi_run, consts.INIT_CANCELED)
                    ]
                )
            )
        self.request_transition(consts.INIT_RUNNING)
