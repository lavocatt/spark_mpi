#!/usr/bin/env python3
import sys
import os
import json
import base64
from main_expe import G5K as Expe
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskProcessor
from yggdrasil.task_lib import Ventilator
from yggdrasil          import Helper


class FirstLevel(Ventilator) :
    def print_data(self, data, prefix):
        decoded = json.loads(data)
        for line in decoded["stdout"] :
            stdout = base64.b64decode(line).decode(self.encoding)
            stdout = stdout.split("output > ")[1]
            mkdir = Ventilator("mkdir",
                    ["mkdir {}/{}".format(G5K.output_dir, stdout)],
                    self.group, end=True, fail_threshold=1, exec_rank=self.exec_rank)
            if stdout.startswith("app") :
                fetch = AppFetcher("AppFetcher",
                        ["ls {}/work/{}".format(G5K.base_path, stdout)],
                        self.group, end=True, exec_rank=self.exec_rank)
            elif stdout.startswith("driver") :
                fetch = Ventilator("scp",
                        ["scp {}:{}/work/{}/st* {}/{}".format(
                            self.node_to_fetch, G5K.base_path, stdout,
                            G5K.output_dir, stdout)],
                        self.group, end=True, fail_threshold=1, exec_rank=self.exec_rank)
                fetch.add_dependency_for_transition(
                        consts.INIT_RUNNING,
                        consts.RUNNING_DONE,
                        mkdir)
            fetch.folder_name   = stdout
            fetch.node_to_fetch = self.node_to_fetch
            self.group.add_dependency_for_transition(
                consts.RUNNING_DONE,
                consts.RUNNING_DONE,
                fetch)
            self.group.add_dependency_for_transition(
                consts.RUNNING_DONE,
                consts.RUNNING_DONE,
                mkdir)
            fetch.add_dependency_for_transition(
                    consts.INIT_RUNNING,
                    consts.RUNNING_DONE,
                    mkdir)
            G5K.processor.add_task(mkdir)
            G5K.processor.add_task(fetch)
            print(stdout)

class AppFetcher(Ventilator) :
    def print_data(self, data, prefix):
        decoded = json.loads(data)
        for line in decoded["stdout"] :
            stdout = base64.b64decode(line).decode(self.encoding)
            stdout = stdout.split("output > ")[1]
            print(stdout)


class AppFetcher(Ventilator) :
    def print_data(self, data, prefix):
        decoded = json.loads(data)
        for line in decoded["stdout"] :
            stdout = base64.b64decode(line).decode(self.encoding)
            stdout = stdout.split("output > ")[1]
            mkdir = Ventilator("mkdir",
                    ["mkdir {}/{}/{}".format(G5K.output_dir, self.folder_name, stdout)],
                    self.group, end=True, fail_threshold=1, exec_rank=self.exec_rank)
            scp = Ventilator("scp",
                    ["scp {}:{}/work/{}/{}/st* {}/{}/{}".format(
                        self.node_to_fetch, G5K.base_path, self.folder_name, stdout,
                        G5K.output_dir, self.folder_name, stdout)],
                    self.group, end=True, fail_threshold=1, exec_rank=self.exec_rank)
            self.group.add_dependency_for_transition(
                consts.RUNNING_DONE,
                consts.RUNNING_DONE,
                mkdir)
            self.group.add_dependency_for_transition(
                consts.RUNNING_DONE,
                consts.RUNNING_DONE,
                scp)
            scp.add_dependency_for_transition(
                    consts.INIT_RUNNING,
                    consts.RUNNING_DONE,
                    mkdir)
            G5K.processor.add_task(mkdir)
            G5K.processor.add_task(scp)
            print(stdout)

class G5K(Expe):

    # Environment
    base_path           = "/var/lib/spark/spark-2.2.0-bin-hadoop2.7"
    username            = "tlavocat"
    nbqueus             = 1
    nodesimu            = 1
    receivers_by_sender = 1
    mpi_process_per_n   = 1
    output_dir          = "/home/{}/Documents/these/".format(username)

    def __init__(self, erebor, ID, node_list, tfile=None):
        G5K.processor = self
        Expe.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        list_slaves  = self.spark_cluster.split(",")[1:]
        slaves_nodes = ",".join(list_slaves)

        slave_group = NumberedGroup("slaves_group", "slave_group", slaves_nodes, "0", "root")
        i = 0
        for slave in list_slaves :
            i+=1
            ls = FirstLevel("ls{}".format(i),
                    ["ls {}/work/".format(G5K.base_path)],
                    slave_group, end=True, exec_rank="{}".format(i))
            ls.node_to_fetch = slave
            ls.add_dependency_for_transition(
                consts.INIT_RUNNING,
                consts.INIT_RUNNING,
                slave_group)
            slave_group.add_dependency_for_transition(
                consts.RUNNING_DONE,
                consts.RUNNING_DONE,
                ls)
            self.tasks.append(ls)
        self.tasks.append(slave_group)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, G5K))
