#!/usr/bin/env python3
import re
from pathlib import Path
import shutil
import sys
import os
sys.path.append(".")
from yggdrasil.erebor       import argless_runner
from main_expe              import G5K as main_executor

class ExpeDriver :

    def __init__(self, spark_path, username, properties_path, sparServer, mqpath,
            mppath, valpath, dmem, emem, ecores, hdfs_root,
            hdfs_path="/var/lib/spark/hadoop-2.7.5",
            base_project = "/home/tlavocat/Documents/these"):
        # Path
        main_executor.base_path    = spark_path
        main_executor.bin_path     = "{}/bin".format(main_executor.base_path)
        main_executor.sbin_path    = "{}/sbin".format(main_executor.base_path)
        main_executor.base_project = base_project
        main_executor.username     = username
        main_executor.extrat_lib   = "{}/jzmq/jzmq-jni/src/main/c++/.libs/".format( main_executor.base_project)
        main_executor.spark_jar    = "{}/spark_mpi/target/simple-project-1.0-jar-with-dependencies.jar".format(main_executor.base_project)
        main_executor.validatorpath=valpath
        main_executor.property_file= properties_path

        # Spark programm
        main_executor.sparkServer  = sparServer
        main_executor.sparkClient  = mqpath
        main_executor.stencyl      = mppath

        # Deployment control
        main_executor.sender_base_port   = 9999 # On wich port should the queue listen on output connexion ?
        main_executor.driver_mem         = dmem
        main_executor.executor_mem       = emem
        main_executor.executor_cores     = ecores

        main_executor.hdfs_path    = hdfs_path
        main_executor.spark_hdfs_root = hdfs_root

    EXPE_LIST = []

    def launch_experiment(self, nb_receivers, nb_mpi_process, nb_queus, nb_nodesimu,
                          spark_cluster_size, producer_buffer, stencyl_buffer,
                          duration, result_base, expe_name,
                          spark_window,
                          block_interval,
                          concurrent_jobs,
                          default_parralelism) :
        # Size of the messages
        main_executor.queue_output_buffer = producer_buffer
        main_executor.stencyl_buffer      = stencyl_buffer
        main_executor.producer_duration   = duration
        main_executor.producer_timeout    = duration + 10000

        # Execution control
        main_executor.receivers_by_sender= nb_receivers
        main_executor.mpi_process_per_n  = nb_mpi_process
        main_executor.nbqueus            = nb_queus
        main_executor.nodesimu           = nb_nodesimu
        main_executor.spark_cluster_size = spark_cluster_size

        main_executor.spark_window              = spark_window
        main_executor.spark_block_interval      = block_interval
        main_executor.spark_concurrent_blocks   = concurrent_jobs
        main_executor.spark_default_parallelism = default_parralelism

        # Prepare folder to embeds results
        base_folder = Path(result_base)
        assert base_folder.is_dir()
        main_executor.output_dir   = "{}/{}".format(result_base, expe_name);
        result_folder = Path(main_executor.output_dir)
        if result_folder.is_dir() :
            try:
                shutil.rmtree(result_folder)
            except:
                try :
                    result_folder.rmdir()
                except:
                    pass
        try :
            result_folder.mkdir()
        except:
            pass

        # Remove old files from message queue
        for f in os.listdir(main_executor.base_project):
            if re.search("output.*.csv", f):
                os.remove(os.path.join(main_executor.base_project , f))
            if re.search("lags.*.csv", f):
                os.remove(os.path.join(main_executor.base_project , f))

        ###########################################################################
        # run the experiment
        argless_runner(main_executor, logv="CRITICAL")
        ###########################################################################
        # Sum up queues output for R processing
        list_input_files = []
        for f in os.listdir(main_executor.base_project):
            if re.search("output.*.csv", f):
                fi = open(os.path.join(main_executor.base_project, f), 'r')
                list_input_files.append(fi.readlines())
                fi.close()

        sumFile = open("{}/sum.csv".format(main_executor.output_dir), "w")
        first   = False
        for inputFile in list_input_files :
            i = 0
            for line in inputFile :
                if i==0 :
                    if not first :
                        sumFile.write(line)
                        first = True
                else :
                    sumFile.write(line)
                i+=1
        sumFile.close()
        ## Move lags
        #list_input_files = []
        #for f in os.listdir(main_executor.base_project):
        #    if re.search("lags.*.csv", f):
        #        fi = open(os.path.join(main_executor.base_project, f), 'r')
        #        list_input_files.append(fi.readlines())
        #        fi.close()

        #sumFile = open("{}/lags.csv".format(main_executor.output_dir), "w")
        #first   = False
        #for inputFile in list_input_files :
        #    i = 0
        #    for line in inputFile :
        #        if i==0 :
        #            if not first :
        #                sumFile.write(line)
        #                first = True
        #        else :
        #            sumFile.write(line)
        #        i+=1
        #sumFile.close()
        # Move batch stats
        driver_list = []
        for f in os.listdir(main_executor.output_dir):
            if re.search("driver.*", f):
                driver_list.append(f)
        driver_list.sort()
        os.rename("{}/{}/batch_infos.csv".format(main_executor.output_dir,
            driver_list[-1]),
            "{}/batch_infos.csv".format(main_executor.output_dir))
        # move missing
        os.rename("{}/missing.csv".format(main_executor.base_project),
            "{}/missing.csv".format(main_executor.output_dir))
        # move th_steps
        os.rename("{}/th_steps.csv".format(main_executor.base_project),
            "{}/th_steps.csv".format(main_executor.output_dir))

        if main_executor.exec_ok :
            self.EXPE_LIST.append("{},{},{},{},{},{},{},{},{},{},{},{},{},".format(
                result_folder.absolute(), nb_receivers, nb_queus, nb_nodesimu,
                spark_cluster_size, nb_mpi_process, producer_buffer,
                stencyl_buffer, duration, spark_window,
                block_interval, concurrent_jobs, default_parralelism))
        return main_executor.exec_ok

    def start_session(self, node_list) :
        main_executor.node_list = node_list
        self.EXPE_LIST.append("{},{},{},{},{},{},{},{},{},{},{},{},{},".format(
            "folder", "receivers", "queues", "nodes_simu",
            "nb_spark_node", "nb_mpi_process", "q_o_buffer",
            "app_buffer", "duration", "spark_window",
            "block_interval", "concurrent_jobs",
            "default_parralelism"))
        # List of nodes for the experiment
    def drive(self, node_list, expe_function) :
        self.start_session()
        expe_function(self)
        self.end_session()

    def end_session(self) :
        # Write R file
        rfile = open("/home/{}/Documents/these/rfile.csv".format(main_executor.username), "w")
        for expe in self.EXPE_LIST :
            rfile.write("{}\n".format(expe))
        rfile.close()

