import expe_driver

hdfs_path   = "/home/user1/bin/spark/hadoop-2.7.6"
spark_home = "/home/user1/bin/spark/spark-2.2.0-bin-hadoop2.7"
username   = "user1"
properties = "/home/user1/properties"
base_project = "/home/user1/Documents/these"
spark_code = "MSDComd2"
queue_exec = "/home/user1/Documents/these/message_queue/build/message_queue"
app_exec   = "/home/user1/Documents/these/mpi_use_cases/CoMD/bin/CoMD-mpi -d /home/user1/Documents/these/mpi_use_cases/CoMD/examples/pots -e -i 1 -j 1 -k 1 -x 40 -y 40 -z 40 -K ij"
valpath    = "/home/user1/Documents/these/validator/build/validator"
driver_mem = 10
execut_mem = 5
execut_cors= 32
hdfs_root  = "."
node_list  = "localhost,B,C,D,E,F"

driver = expe_driver.ExpeDriver(spark_home,
                username,
                properties,
                spark_code,
                queue_exec,
                app_exec,
                valpath,
                driver_mem,
                execut_mem,
                execut_cors,
                hdfs_root,
                hdfs_path=hdfs_path,
                base_project=base_project);

driver.start_session(node_list)

path ="/home/user1/Documents/these/expes/0/sum.csv"
nb_receivers = 1
nb_message_q = 1
nb_node_simu = 1
nb_spark_node = 3
nb_mpi_process = 1
q_o_buffer = (4000+9*8)
app_buffer = 100
duration   = 60000
spark_window = 20000
block_interval = 4000
concurrent_jobs = 1
default_parralelism = 16

driver.launch_experiment(nb_receivers,
                         nb_mpi_process,
                         nb_message_q,
                         nb_node_simu,
                         nb_spark_node,
                         q_o_buffer,
                         app_buffer,
                         duration,
                         "/home/user1/Documents/these/expes/",
                         0,
                         spark_window,
                         block_interval,
                         concurrent_jobs,
                         default_parralelism)
driver.end_session()
