from yggdrasil.task_lib import Task
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskBag
from yggdrasil.task_lib import Ventilator
from yggdrasil import consts
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class SparkStreaming(Task) :

    def __init__(self, name,
                 property_file,
                 node_list,
                 base_project,
                 spark_path,
                 bin_path,
                 sbin_path,
                 extrat_lib,
                 executor_cores,
                 driver_mem,
                 executor_mem,
                 spark_jar,
                 spark_app,
                 datasources,
                 receivers,
                 spark_window,
                 spark_block_interval,
                 spark_hdfs_root,
                 spark_default_parallelism,
                 spark_concurrent_blocks,
                 dir_gather_spark_logs,
                 spark_app_extra_param = "",
                 java_path = "/home/user1/bin/spark/jdk1.8.0_172",
                 jail_name = "spark_cluster",
            timeout=-1, root_r=0, root_n="root"):
        Task.__init__(self, name, timeout, root_r, root_n)
        self.spark_path                = spark_path
        self.sbin_path                 = sbin_path
        self.node_list                 = node_list
        self.base_project              = base_project
        self.extrat_lib                = extrat_lib
        self.executor_cores            = executor_cores
        self.driver_mem                = driver_mem
        self.property_file             = property_file
        self.bin_path                  = bin_path
        self.spark_app                 = spark_app
        self.executor_mem              = executor_mem
        self.spark_jar                 = spark_jar
        self.datasources               = datasources
        self.receivers                 = receivers
        self.spark_window              = spark_window
        self.spark_block_interval      = spark_block_interval
        self.spark_hdfs_root           = spark_hdfs_root
        self.spark_default_parallelism = spark_default_parallelism
        self.spark_concurrent_blocks   = spark_concurrent_blocks
        self.spark_app_extra_param     = spark_app_extra_param
        self.jail_name                 = jail_name
        self.dir_gather_spark_logs     = dir_gather_spark_logs
        self.java_path                 = java_path
        self.path = ""
        self.java_home = ""
        if self.java_path is not "" :
            self.java_home = "export JAVA_HOME={};".format(self.java_path)
            self.path = "export PATH={}/bin:$PATH; ".format(self.java_path)
        self.spark_started = True

    def init_state(self) :
        group_bag = TaskBag("bag1")
        lst_tasks = []
        lst_tasks.append(group_bag)
        # Define paths
        # start the master
        # Create the group master_node with the first node of the node list
        # dedicated to spark execution. Upon group creation, the start master
        # command is callded on the node.
        master_node  = ",".join(self.node_list.split(",")[:1])
        self.master_group = NumberedGroup("master_group", "master_group", master_node, "0", "root")
        group_bag.add_task(self.master_group)
        start_master = Ventilator("start_master", [("{}{}{}/start-master.sh -h {} "
                                          "-p {}").format(
                                                    self.path,
                                                    self.java_home,
                                                    self.sbin_path,
                                                    master_node,
                                                    7077)
                                        ],
                       self.master_group, False, -1, 0, consts.encoding, "1")
        # the start_master task runs when the master group runs
        start_master.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                        DependencyEvent(group_bag, consts.INIT_RUNNING)
                    ]
                    )
                )
        start_master.set_trigger_for_transition(consts.INIT_CANCELED,
                Dependency(Dependency.OR,
                    [
                        DependencyEvent(group_bag, consts.INIT_ERROR),
                        DependencyEvent(group_bag, consts.RUNNING_ERROR)
                    ]
                    )
                )
        lst_tasks.append(self.master_group)
        lst_tasks.append(start_master)
        group_bag.set_all_trigger_for_transition(consts.RUNNING_CANCELED,
                Dependency(Dependency.OR,
                    [
                        DependencyEvent(start_master, consts.RUNNING_ERROR)
                    ]
                    )
                )
        # For the rest of the nodes dedicated to spark, we call the start-slave
        # program. The master IP and Port are given to the slaves. Also the
        # amount of ram to use and max number of cores. The task itself will
        # start after the master bootstrap is done.

        slaves_nodes = ",".join(self.node_list.split(",")[1:])
        print("Slaves nodes to host executors {}".format(slaves_nodes))
        self.list_slave_group = []
        i=0
        nb_slaves        = len(slaves_nodes.split(","))
        slave_bag = TaskBag("slave_bag1")
        lst_tasks.append(slave_bag)
        for slave_node in slaves_nodes.split(",") :
            slave_group = NumberedGroup("slave_group{}".format(i), "slave_group{}".format(i), slave_node, "0", "root")
            i+=1
            # The slave group is kept until the end of the execution to stop the
            # slaves
            start_slave = Ventilator("start_slave{}".format(i),
                                     [(
                                         "{}{}{}/spark_mpi/launcher {} \""
                                        "{}/start-slave.sh "
                                         "-h {} "
                                         "-p {} "
                                         "-c {} "
                                         "-m {}G "
                                         " --properties-file {} "
                                         "spark://{}:{}\""
                                        ).format(
                                            self.path,
                                            self.java_home,
                                            self.base_project,
                                            self.extrat_lib,
                                            self.sbin_path,
                                            slave_node,
                                            7077,
                                            self.executor_cores,
                                            self.driver_mem,
                                            self.property_file,
                                            master_node,
                                            7077)],
                    slave_group, False, -1, 0, consts.encoding, "1")
            start_slave.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                       DependencyEvent(group_bag, consts.INIT_RUNNING),
                       DependencyEvent(start_master, consts.RUNNING_DONE)
                   ]
                   )
               )
            group_bag.add_task(slave_group)
            slave_bag.add_task(start_slave)
            lst_tasks.append(start_slave)
            lst_tasks.append(slave_group)
            self.list_slave_group.append(slave_group)
        slave_bag.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                       slave_bag.get_dependency_for_transition(consts.INIT_RUNNING),
                       DependencyEvent(group_bag, consts.INIT_RUNNING),
                       DependencyEvent(start_master, consts.RUNNING_DONE)
                   ]
                   )
               )
        slave_bag.set_all_trigger_for_transition(consts.INIT_CANCELED,
                Dependency(Dependency.OR,
                    [
                       DependencyEvent(start_master, consts.INIT_ERROR),
                       DependencyEvent(start_master, consts.RUNNING_ERROR)
                   ]
                   )
               )

        # Create the group that will hold spark
        spark_cluster= MPIJail(self.jail_name,self.jail_name, self.node_list, '0', 'root', tcp=True)

        new_slaves_nodes = []
        for node in slaves_nodes.split(",") :
            new_slaves_nodes.append(node)

        slaves_nodes = ",".join(new_slaves_nodes)

        # Start the spark application server
        # This task will start after each spark worker is launched.
        # as the call on spark-submit will return before the application is
        # really done, keep the server group alive in order to keep transmit
        # messages.
        hr = "."
        if "." is not self.spark_hdfs_root :
            hr = "hdfs://{}:9000/".format(self.spark_hdfs_root)
        spark_submit = MPIExecutor('spark_submit', ("{}{}{}/spark-submit "
                         "--conf spark.executor.extraJavaOptions=\"-XX:+UseG1GC \" "
                         #"--conf spark.executor.extraJavaOptions=\"-javaagent:/home/user1/.spark-flamegraph/statsd-jvm-profiler/target/statsd-jvm-profiler-2.1.1-SNAPSHOT-jar-with-dependencies.jar=server=localhost,port=8086,reporter=InfluxDBReporter,database=profiler,username=profiler,password=profiler,prefix=sparkapp,tagMapping=spark\" "
                         "--driver-library-path={} "
                         "--class \"{}\" "
                         "--master spark://{}:{} "
                         "--deploy-mode cluster "
                         "--driver-memory {}G "
                         "--executor-memory {}G "
                         #"--jars /home/user1/.spark-flamegraph/statsd-jvm-profiler/target/statsd-jvm-profiler-2.1.1-SNAPSHOT-jar-with-dependencies.jar "
                         "{} "
                         " --DATASOURCES {} --RECEIVERS {} "
                         " --WINDOWTIME {} --BLOCKINTERVAL {}"
                         " --CONCBLOCK {} --HDFS {}"
                         " --PARAL {}{}").format(
                                 self.path,
                                 self.java_home,
                                 self.bin_path,
                                 self.extrat_lib,
                                 self.spark_app,
                                 master_node, 7077,
                                 self.driver_mem,
                                 self.executor_mem,
                                 self.spark_jar,
                                 self.datasources,
                                 self.receivers,
                                 self.spark_window,
                                 self.spark_block_interval,
                                 self.spark_concurrent_blocks,
                                 hr,
                                 self.spark_default_parallelism,
                                 self.spark_app_extra_param
                                 # Additional parameters --CLIENTS {} --TEMPON {} 
                                 #self.mpi_process_per_n,
                                 #self.stencyl_buffer
                                 ),
                 spark_cluster, True, -1, consts.encoding, True, "1")

        spark_cluster.set_dependency_for_transition(consts.INIT_RUNNING,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(slave_bag, consts.RUNNING_DONE)
               ]
               )
           )
        spark_cluster.set_trigger_for_transition(consts.INIT_CANCELED,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(slave_bag, consts.INIT_CANCELED)
               ]
               )
           )
        spark_cluster.set_dependency_for_transition(consts.RUNNING_DONE,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(self, consts.RUNNING_DONE)
               ]
               )
           )

        spark_submit.set_dependency_for_transition(consts.INIT_RUNNING,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(spark_cluster, consts.INIT_RUNNING),
                   DependencyEvent(slave_bag, consts.RUNNING_DONE)
               ]
               )
           )
        spark_submit.set_trigger_for_transition(consts.INIT_CANCELED,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(slave_bag, consts.INIT_CANCELED)
               ]
               )
           )

        lst_tasks.append(spark_cluster)
        lst_tasks.append(spark_submit)

        self.set_dependency_for_transition(consts.INIT_RUNNING,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(spark_submit, consts.RUNNING_DONE)
               ]
               )
           )
        # Do not execute the if the whole execution cannot exist
        self.set_trigger_for_transition(consts.INIT_CANCELED,
            Dependency(Dependency.OR,
                [
                   DependencyEvent(spark_submit, consts.INIT_CANCELED),
                   DependencyEvent(spark_submit, consts.RUNNING_ERROR),
               ]
               )
           )

        self.framework.add_start_all(lst_tasks)
        self.request_transition(consts.INIT_RUNNING)
        self.spark_started = True

    def runing_state(self) :
        self.execute_when_transition_transition_possible(consts.RUNNING_DONE,
                                                         self.stop_spark)

    def get_files(self) :
        lst_tasks = []
        master_node = self.node_list.split(",")[0]
        last = None
        i=0
        for slave_group in self.list_slave_group :
            i+=1;
            rsync = Ventilator("rsync{}".format(i), [(
                    "rsync -ar  --exclude='*.jar' --exclude='receivedBlockMetadata' --exclude='check*' "
                    "{}/work/* "
                    "{}:{}/"
                ).format(self.spark_path,
                         master_node,self.dir_gather_spark_logs)],
                slave_group,
                fail_threshold=1,
                exec_rank="1")
            if last != None :
                rsync.set_dependency_for_transition(consts.INIT_RUNNING,
                        Dependency(
                            Dependency.AND,
                            [
                                DependencyEvent(last, consts.RUNNING_DONE)
                            ]
                        )
                    )
            last = rsync
            lst_tasks.append(rsync)
            self.set_dependency_for_transition(consts.RUNNING_DONE,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(last, consts.RUNNING_DONE)
                    ]
                )
            )
        self.framework.add_start_all(lst_tasks)
        self.execute_when_transition_transition_possible(consts.RUNNING_DONE,
                                                         self.remove_files)

    def remove_files(self) :
        lst_tasks = []
        last = None
        i=0
        for slave_group in self.list_slave_group :
            i+=1;
            rm = Ventilator("rm{}".format(i), [("rm -rf {}/work/*").format(self.spark_path)],
                slave_group,
                end=True,
                fail_threshold=1,
                exec_rank="1")
            if last != None :
                rm.set_dependency_for_transition(consts.INIT_RUNNING,
                        Dependency(
                            Dependency.AND,
                            [
                                DependencyEvent(last, consts.RUNNING_DONE)
                            ]
                        )
                    )
            last = rm
            lst_tasks.append(rm)
            self.set_dependency_for_transition(consts.RUNNING_DONE,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(last, consts.RUNNING_DONE)
                    ]
                )
            )
        self.framework.add_start_all(lst_tasks)
        self.request_transition(consts.RUNNING_DONE)

    def error_state(self) :
        if self.spark_started :
            self.stop_spark()

    def timeout_state(self) :
        if self.spark_started :
            self.stop_spark()

    def canceled_state(self) :
        if self.spark_started :
            self.stop_spark()

    def stop_spark(self) :
        if self.master_group.state == consts.RUNNING :
            lst_tasks=[]
            # Application ending
            # Stop the spark master
            stop_master = Ventilator("stop_master",
                    [("{}/stop-master.sh").format(self.sbin_path)],
                                     self.master_group, True, -1, 0, consts.encoding, "1")
            lst_tasks.append(stop_master)
            i = 0;
            list_stop_slave = []
            for slave_group in self.list_slave_group :
                i+=1;
                stop_slave = Ventilator("stop_slave{}".format(i),
                                         [("{}/stop-slave.sh"
                                            ).format(self.sbin_path)],
                    slave_group, False, -1, 0, consts.encoding, "1")
                stop_slave.set_dependency_for_transition(consts.INIT_RUNNING,
                        Dependency(
                            Dependency.AND,
                            [
                                DependencyEvent(stop_master, consts.RUNNING_DONE)
                            ]
                        )
                    )
                list_stop_slave.append(DependencyEvent(stop_slave,
                    consts.RUNNING_DONE))
                lst_tasks.append(stop_slave)
            self.set_dependency_for_transition(
                consts.RUNNING_DONE,
                Dependency(Dependency.AND, list_stop_slave))
            self.framework.add_start_all(lst_tasks)
        self.execute_when_transition_transition_possible(consts.RUNNING_DONE,
                                                         self.get_files)
