import sys
import os
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskProcessor
from yggdrasil.task_lib import Ventilator
from yggdrasil.task_lib import TaskBag
from yggdrasil          import Helper
from spark_group        import SparkStreaming
from mpi_run            import MPIRun
from ranked_program     import RankedApp
from hdfs_group         import HDFS
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class G5K(TaskProcessor):

    # Environment
    hdfs_path    = "/var/lib/spark/hadoop-2.7.5"
    base_path    = "/var/lib/spark/spark-2.2.0-bin-hadoop2.7"
    bin_path     = "{}/bin".format(base_path)
    sbin_path    = "{}/sbin".format(base_path)
    username     = "tlavocat"
    base_project = "/home/{}/Documents/these".format(username)
    output_dir   = "/home/{}/Documents/these/expes/0/".format(username)
    extrat_lib   = "{}/jzmq/jzmq-jni/src/main/c++/.libs/".format(base_project)
    spark_jar    = "{}/spark_mpi/target/simple-project-1.0-jar-with-dependencies.jar".format(base_project)
    property_file= "/home/tlavocat/properties"
    # Perf options
    # Program
    sparkServer  = "StatelessSUMServer"
    validatorpath= "/home/user1/Documents/these/validator/build/validator"
    sparkClient  = "/home/user1/Documents/these/message_queue/build/message_queue"
    stencyl      = "/home/user1/Documents/these/mpi_use_cases/stencyl_app/build/stencyl"
    # Execution control
    stencyl_buffer       = 100
    queue_output_buffer  = (1000*8)
    producer_duration    = 10000
    # expe setup
    nbqueus             = 1
    nodesimu            = 1
    receivers_by_sender = 1
    mpi_process_per_n   = 1
    sender_base_port    = 9999
    node_list           = ""
    spark_cluster_size  = 1
    # perf tuning
    driver_mem                = 10
    executor_mem              = 5
    executor_cores            = 32
    spark_window              = 1000
    spark_block_interval      = 200
    spark_concurrent_blocks   = 16
    spark_hdfs_root           = "."
    spark_default_parallelism = 16
    exec_ok                   = False

    def __init__(self, erebor, ID, g_node_list, tfile=None):
        node_list = G5K.node_list
        G5K.producer_timeout = int(G5K.producer_duration + (G5K.producer_duration/2))
        """
        la node_list est exploitée comme suit :
        * les n-1 premiers noeuds sont dédiés à SPARK
        * le dernier noeud est dédié au producteur de données

        Dans les n-1 premiers noeuds, le premier accueillera le master et les
        autres accueilleront les slaves.
        """
        self.helper = Helper();
        self.node_list = self.helper.build_list(node_list)
        assert G5K.spark_cluster_size+G5K.nodesimu+G5K.nbqueus <= len(self.node_list)
        self.spark_cluster = ",".join(self.node_list[:G5K.spark_cluster_size])
        self.producers     = self.node_list[G5K.spark_cluster_size:G5K.spark_cluster_size+G5K.nodesimu+G5K.nbqueus]
        self.queue = ",".join(self.producers[:G5K.nbqueus])
        self.mpi   = ",".join(self.producers[G5K.nbqueus:])
        print()
        print("{} : node list ".format(self.node_list))
        print("{} : spark cluster ".format(self.spark_cluster))
        print("{} : queues ".format(self.queue))
        print("{} : mpi ".format(self.mpi))
        print()

        TaskProcessor.__init__(self, erebor, ID, tfile)
        G5K.processor = self

    def register_tasks(self) :

        sparkStreaming = SparkStreaming("sparkStreaming",
                G5K.property_file,
                self.spark_cluster,
                G5K.base_project,
                G5K.base_path,
                G5K.bin_path,
                G5K.sbin_path,
                G5K.extrat_lib,
                G5K.executor_cores,
                G5K.driver_mem,
                G5K.executor_mem,
                G5K.spark_jar,
                G5K.sparkServer,
                self.queue,
                G5K.receivers_by_sender,
                G5K.spark_window,
                G5K.spark_block_interval,
                G5K.spark_hdfs_root,
                G5K.spark_default_parallelism,
                G5K.spark_concurrent_blocks,
                G5K.output_dir,
                jail_name = "spark_cluster",
                spark_app_extra_param=" --CLIENTS {} --TEMPON {}".format(
                                                  G5K.mpi_process_per_n,
                                                  G5K.stencyl_buffer)
                )
        if "." is not G5K.spark_hdfs_root :
            hdfs           = HDFS(self.spark_cluster, G5K.hdfs_path)
            sparkStreaming.set_trigger_for_transition(consts.IDDLE_CANCEL,
                       Dependency(Dependency.OR,
                           [
                               DependencyEvent(hdfs, consts.INIT_ERROR)
                           ]
                       )
                   )
            sparkStreaming.set_dependency_for_transition(consts.IDLE_INIT,
                       Dependency(Dependency.OR,
                           [
                               DependencyEvent(hdfs, consts.RUNNING_DONE)
                           ]
                       )
                   )
            self.tasks.append(hdfs)

        base_port = G5K.sender_base_port;
        list_base_ports = []
        for r in range(1, G5K.nbqueus+1) :
            list_base_ports.append(str((base_port - 1111) + ((r-1)*(2*G5K.receivers_by_sender))))

        # the queues wait for spark streaming to be launched
        def get_options(rank) :
            lp = (base_port) + ((rank-1)*(2*G5K.receivers_by_sender))
            return"-U {} -R {} -S {} -T {} -D {} -H {} -B {}".format(
                                            G5K.username,
                                            rank,
                                            "spark_cluster",
                                            G5K.queue_output_buffer,
                                            G5K.producer_duration,
                                            G5K.receivers_by_sender,
                                            lp)
        def get_validator_options(rank) :
            return"-U {} -D {} -H {}".format(
                                            G5K.username,
                                            G5K.producer_duration*2,
                                            G5K.spark_cluster_size-2)
        validator = RankedApp("validatorapp",
                              self.node_list[0],
                              G5K.validatorpath,
                              1,
                              get_validator_options,
                              jail_name="validator")
        validator.set_dependency_for_transition(consts.IDLE_INIT,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(sparkStreaming, consts.IDLE_INIT)
               ]
               )
           )
        validator.set_trigger_for_transition(consts.IDDLE_CANCEL,
            Dependency(Dependency.OR,
                [
                   DependencyEvent(sparkStreaming, consts.INIT_CANCELED),
                   DependencyEvent(sparkStreaming, consts.IDDLE_CANCEL),
               ]
               )
           )
        self.tasks.append(validator)
        rankedApp = RankedApp("queues",
                              self.queue,
                              G5K.sparkClient,
                              G5K.nbqueus,
                              get_options,
                              jail_name="producer_group")
        rankedApp.set_dependency_for_transition(consts.IDLE_INIT,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(sparkStreaming, consts.INIT_RUNNING)
               ]
               )
           )
        rankedApp.set_trigger_for_transition(consts.IDDLE_CANCEL,
            Dependency(Dependency.OR,
                [
                   DependencyEvent(sparkStreaming, consts.INIT_CANCELED),
                   DependencyEvent(sparkStreaming, consts.IDDLE_CANCEL),
               ]
               )
           )

        # MPI run function wait until the sparkStreaming is launched to start
        mpiRun = MPIRun("mpi_runner",
                        self.mpi,
                        G5K.stencyl,
                        "-np {} --bynode --oversubscribe ".format( G5K.mpi_process_per_n),
                        " -S {} -Q {} -E {} -W {} -T {} -Z {}".format(
                                "producer_group", self.queue,
                                ",".join(list_base_ports), len(list_base_ports),
                                G5K.producer_duration, G5K.stencyl_buffer
                            )
                        )
        mpiRun.set_dependency_for_transition(consts.IDLE_INIT,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(sparkStreaming, consts.INIT_RUNNING)
               ]
               )
           )
        mpiRun.set_trigger_for_transition(consts.IDDLE_CANCEL,
            Dependency(Dependency.OR,
                [
                   DependencyEvent(sparkStreaming, consts.INIT_CANCELED),
                   DependencyEvent(sparkStreaming, consts.IDDLE_CANCEL),
               ]
               )
           )

        # As the queue wait until spark has done its work to return, its
        # termination implies sparkStreaming termination.
        sparkStreaming.set_dependency_for_transition( consts.RUNNING_DONE,
            Dependency(Dependency.AND,
                [
                   Dependency(Dependency.OR,
                       [
                           DependencyEvent(mpiRun, consts.RUNNING_DONE),
                           DependencyEvent(mpiRun, consts.INIT_ERROR)
                       ]
                   ),
                   Dependency(Dependency.OR,
                       [
                           DependencyEvent(rankedApp, consts.RUNNING_DONE),
                           DependencyEvent(rankedApp, consts.INIT_ERROR)
                       ]
                   ),
               ]
               )
           )

        def execution_ok() :
            G5K.exec_ok = True
        mpiRun.execute_when_transition_transition_possible(consts.RUNNING_DONE, execution_ok)

        self.tasks.append(rankedApp)
        self.tasks.append(mpiRun)
        self.tasks.append(sparkStreaming)

        if "." is not G5K.spark_hdfs_root :
            stop_hdfs = HDFS(self.spark_cluster, G5K.hdfs_path, start = False)
            stop_hdfs.set_dependency_for_transition(consts.IDLE_INIT,
                       Dependency(Dependency.OR,
                           [
                               DependencyEvent(sparkStreaming, consts.RUNNING_DONE)
                           ]
                       )
                   )
            self.tasks.append(stop_hdfs)
