import os
import expe_driver

hdfs_path  = "/var/lib/spark/hadoop-2.7.5"
spark_home = "/var/lib/spark/spark-2.2.0-bin-hadoop2.7"
username   = "tlavocat"
properties = "/home/tlavocat/properties"
spark_code = "SUMServer"
queue_exec = "/home/tlavocat/Documents/these/message_queue/build/message_queue"
app_exec   = "/home/tlavocat/Documents/these/toy_stencyl_app/build/stencyl"
driver_mem = 100
execut_mem = 100
execut_cors= 64
print(os.environ['YGGDRASIL_NODE_LIST'])
nlst = os.environ['YGGDRASIL_NODE_LIST'].split(",")
hdfs_root  = nlst[0]
node_list  = ",".join(nlst)

driver = expe_driver.ExpeDriver(spark_home,
                username,
                properties,
                spark_code,
                queue_exec,
                app_exec,
                driver_mem,
                execut_mem,
                execut_cors,
                hdfs_root,
                hdfs_path);

path ="/home/tlavocat/Documents/these/expes/0/sum.csv"
nb_receivers        = 1
nb_message_q        = 1
nb_node_simu        = 1
nb_spark_node       = 2+1
nb_mpi_process      = 144
base_q_o_buffer     = 1000000
q_o_buffer          = (base_q_o_buffer*4+nb_mpi_process*8)
app_buffer          = 10000
duration            = 60000
spark_window        = 1000
block_interval      = 200
concurrent_jobs     = 32
default_parralelism = 32

nb_expe           = 1
param_to_explore  = "number of nodes"

rec = [4, 8, 8, 5]
meq = [1, 1, 2, 4]
mpi = [4, 4, 4, 4]
exe = [1, 2, 4, 8]
param_exploration = rec
print(param_exploration)

driver.start_session(node_list)
i = 0
k = 0
for param in param_exploration :
    for j in range(0,nb_expe) :
        nb_spark_node = 2+exe[k]
        nb_receivers  = rec[k]
        nb_node_simu  = mpi[k]
        nb_message_q  = meq[k]
        path =  "/home/tlavocat/Documents/these/expes/{}/sum.csv".format(i)
        #default_parralelism = exe[param]*23
        if(driver.launch_experiment(nb_receivers,
                                 nb_mpi_process,
                                 nb_message_q,
                                 nb_node_simu,
                                 nb_spark_node,
                                 q_o_buffer,
                                 app_buffer,
                                 duration,
                                 "/home/tlavocat/Documents/these/expes/",
                                 i,
                                 spark_window,
                                 block_interval,
                                 concurrent_jobs,
                                 default_parralelism) == True) :
            # print parameters
            i = i +1
        else :
            print("error with this setup, cancelling operations.")
            break
driver.end_session()
