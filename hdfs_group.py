import base64
import json
from yggdrasil.task_lib import Task
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskBag
from yggdrasil.task_lib import Ventilator
from yggdrasil import consts
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent



class HDFS(Task) :

    def __init__(self,
                 node_list,
                 hdfs_path="/var/lib/spark/hadoop-2.7.5",
                 timeout=-1, root_r=0, root_n="root", start=True):
        Task.__init__(self, "hdfs_starter{}".format(start), timeout, root_r, root_n)
        self.node_list                 = node_list
        self.need_start = start
        self.core_site_xml = (
                "<configuration>"
                "    <property>"
                "        <name>fs.default.name</name>"
                "        <value>hdfs://{}:9000/</value>"
                "    </property>"
                "</configuration>".format(
                    self.node_list.split(",")[0]))
        self.hdfs_site_xml = (
                "<configuration>"
                "    <property>"
                "        <name>dfs.namenode.name.dir</name>"
                "        <value>/tmp/hadoop/</value>"
                "    </property>"
                "    <property>"
                "        <name>dfs.datanode.data.dir</name>"
                "        <value>/tmp/hadoop/</value>"
                "    </property>"
                "    <property>"
                "        <name>dfs.replication</name>"
                "        <value>1</value>"
                "    </property>"
                "    <property>"
                "        <name>dfs.webhdfs.enabled</name>"
                "        <value>true</value>"
                "    </property>"
                "    <property>"
                "        <name>dfs.permissions.enabled</name>"
                "        <value>false</value>"
                "    </property>"
                "</configuration>"
                )
        self.create_hadoop_config_folder = "mkdir /tmp/hadoop_conf"
        self.create_hadoop_folder        = "mkdir /tmp/hadoop"
        self.clear_hadoop_forder         = "rm -rf /tmp/hadoop/*"
        self.start_name_node = (
                "export JAVA_HOME=$(readlink -f /usr/bin/java | sed 's:bin/java::'); "
                "export HADOOP_PREFIX={}; "
                "export HADOOP_CONF_DIR=/tmp/hadoop_conf; "
                "$HADOOP_PREFIX/bin/hdfs namenode -format -force; "
                "$HADOOP_PREFIX/sbin/hadoop-daemon.sh --config $HADOOP_CONF_DIR "
                "--script hdfs start namenode".format(
                    hdfs_path)
                )
        self.start_data_node = (
                "export JAVA_HOME=$(readlink -f /usr/bin/java | sed 's:bin/java::'); "
                "export HADOOP_PREFIX={}; "
                "export HADOOP_CONF_DIR=/tmp/hadoop_conf/; "
                "$HADOOP_PREFIX/sbin/hadoop-daemon.sh --config $HADOOP_CONF_DIR "
                "--script hdfs start datanode".format(
                    hdfs_path)
                )
        self.stop_name_node = (
                "export JAVA_HOME=$(readlink -f /usr/bin/java | sed 's:bin/java::'); "
                "export HADOOP_PREFIX={}; "
                "export HADOOP_CONF_DIR=/tmp/hadoop_conf; "
                "$HADOOP_PREFIX/bin/hdfs namenode -format -force; "
                "$HADOOP_PREFIX/sbin/hadoop-daemon.sh --config $HADOOP_CONF_DIR "
                "--script hdfs stop namenode".format(
                    hdfs_path)
                )
        self.stop_data_node = (
                "export JAVA_HOME=$(readlink -f /usr/bin/java | sed 's:bin/java::'); "
                "export HADOOP_PREFIX={}; "
                "export HADOOP_CONF_DIR=/tmp/hadoop_conf/; "
                "$HADOOP_PREFIX/sbin/hadoop-daemon.sh --config $HADOOP_CONF_DIR "
                "--script hdfs stop datanode".format(
                    hdfs_path)
                )

    def init_state(self) :
        lst_tasks = []
        master_node  = ",".join(self.node_list.split(",")[:1])
        self.master_group = NumberedGroup("master_group", "master_group",
                                          master_node, "0", "root")

        slaves_nodes = ",".join(self.node_list.split(",")[1:])
        hdfs_start_error   = []
        hdfs_start_running = []
        self.list_slave_group          = []
        i=0
        nb_slaves        = len(slaves_nodes.split(","))
        for slave_node in slaves_nodes.split(",") :
            i+=1
            start_group = NumberedGroup("start_group{}".format(i),
                    "start_group{}".format(i), slave_node, "0", "root")
            lst_tasks.append(start_group)
            self.list_slave_group.append(start_group)
            start_group.set_dependency_for_transition(consts.INIT_RUNNING,
                        Dependency(Dependency.AND,
                            [
                                DependencyEvent(self.master_group, consts.INIT_RUNNING)
                            ]
                        )
                    )
            start_group.set_trigger_for_transition(consts.INIT_ERROR,
                    Dependency(
                        Dependency.OR,
                        [
                            Dependency(Dependency.OR,
                                hdfs_start_error),
                            Dependency(
                                Dependency.AND,
                                [
                                    DependencyEvent(self.master_group, consts.INIT_ERROR)
                                ]
                            )
                        ]
                    )
                )
            hdfs_start_error.append(DependencyEvent(start_group,
                consts.INIT_ERROR))
            hdfs_start_running.append(DependencyEvent(start_group,
                consts.INIT_RUNNING))
        self.set_trigger_for_transition(consts.INIT_CANCELED,
                Dependency(
                    Dependency.OR,
                    hdfs_start_error
                )
            )
        self.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    hdfs_start_running
                )
            )
        self.request_transition(consts.INIT_RUNNING)
        lst_tasks.append(self.master_group)
        self.framework.add_start_all(lst_tasks)

    def stop(self) :
        self.request_transition(consts.INIT_RUNNING)
        self.master_group.request_transition(consts.RUNNING_DONE)
        for start_group in self.list_slave_group :
            start_group.request_transition(consts.RUNNING_DONE)

    def runing_state(self):
        self.start_hdfs()

    def start_hdfs(self) :
        lst_tasks = []
        if self.need_start :
            start_hdfs = Ventilator("start_namenode",
                                [
                                    self.start_name_node,
                                    "echo '{}' > /tmp/hadoop_conf/hdfs-site.xml".format(self.hdfs_site_xml),
                                    "echo '{}' > /tmp/hadoop_conf/core-site.xml".format(self.core_site_xml),
                                    self.create_hadoop_config_folder,
                                    self.clear_hadoop_forder,
                                    self.create_hadoop_folder,
                                ],
                             self.master_group,
                             end=True,
                             exec_rank="1",
                             fail_threshold=2)
        else :
            start_hdfs = Ventilator("stop_namenode",
                                [self.stop_name_node],
                             self.master_group,
                             end=True,
                             exec_rank="1",
                             fail_threshold=2)
        lst_tasks.append(start_hdfs)
        # For the rest of the nodes dedicated to spark, we call the start-slave
        # program. The master IP and Port are given to the slaves. Also the
        # amount of ram to use and max number of cores. The task itself will
        # start after the master bootstrap is done.

        slaves_nodes = ",".join(self.node_list.split(",")[1:])
        dependencies_done = []
        i=0
        nb_slaves        = len(slaves_nodes.split(","))
        for start_group in self.list_slave_group :
            i+=1
            if self.need_start :
                start_datanode = Ventilator("start_datanode{}".format(i),
                                [
                                    self.start_data_node,
                                    "echo '{}' > /tmp/hadoop_conf/core-site.xml".format(self.core_site_xml),
                                    "echo '{}' > /tmp/hadoop_conf/hdfs-site.xml".format(self.hdfs_site_xml),
                                    self.clear_hadoop_forder,
                                    self.create_hadoop_folder,
                                    self.create_hadoop_config_folder,
                                ],
                        start_group, end=True, exec_rank="1", fail_threshold=2)
            else :
                start_datanode = Ventilator("stop_datanode{}".format(i),
                                [self.stop_data_node],
                        start_group, end=True, exec_rank="1", fail_threshold=2)
            start_datanode.set_dependency_for_transition(consts.INIT_RUNNING,
                Dependency(Dependency.AND,
                    [
                       DependencyEvent(start_hdfs, consts.RUNNING_DONE),
                   ]
                   )
               )
            lst_tasks.append(start_datanode)
            dependencies_done.append(DependencyEvent(start_datanode,
                consts.RUNNING_DONE))
        self.set_dependency_for_transition(consts.INIT_RUNNING,
            Dependency(Dependency.AND,
                [
                   DependencyEvent(start_hdfs, consts.INIT_RUNNING)
                ]
              )
           )
        self.set_dependency_for_transition(consts.RUNNING_DONE,
            Dependency(Dependency.AND, dependencies_done)
           )
        self.framework.add_start_all(lst_tasks)
        self.request_transition(consts.RUNNING_DONE)
